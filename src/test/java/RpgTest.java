import org.ffernanl.Character;
import org.ffernanl.Faction;
import org.ffernanl.FightType;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RpgTest {

    @Test
    public void calculateDamageSameLevels(){
        Character char1 = new Character();
        Character char2 = new Character();

        assertEquals(200,char1.calculateDamage(200,char2));
    }
    @Test
    public void calculateDamageAtLeast5LevelsAbove(){
        Character char1 = new Character();
        Character char2 = new Character();

        char1.setLevel(20);
        assertEquals(300,char1.calculateDamage(200,char2));
    }

    @Test
    public void calculateDamageAtLeast5LevelsBelow(){
        Character char1 = new Character();
        Character char2 = new Character();

        char2.setLevel(20);
        assertEquals(100,char1.calculateDamage(200,char2));
    }

    @Test
    public void calculateDamageOutOfRange(){
        Character char1 = new Character();
        Character char2 = new Character();

        char2.setFightType(FightType.RANGE);
        char2.setPosition(20);
        assertEquals(0,char1.calculateDamage(100,char2));
    }

    @Test
    public void isAbleToGetHeal(){
        Character char1 = new Character();
        char1.setHealth(20);
        assertTrue(char1.heal(char1));
    }

    @Test
    public void isAlly(){
        Character char1 = new Character();
        Character char2 = new Character();
       char1.joinFaction(new Faction("Blue"));
       char1.joinFaction(new Faction("Red"));
       char2.joinFaction(new Faction("Red"));

        Assert.assertTrue(char1.isAlly(char2));
    }
}
