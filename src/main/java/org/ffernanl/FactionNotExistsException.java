package org.ffernanl;

public class FactionNotExistsException extends Exception {
    public FactionNotExistsException(String message) {
        super(message);
    }
}
