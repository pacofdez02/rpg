package org.ffernanl;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Character c1 = new Character();
        Character c2 = new Character();

        c1.setHealth(500);
        c2.setHealth(500);

        c1.setLevel(20);
        c1.dealDamage(700,c2);


        System.out.println(c1.getFightType());
        System.out.println(c1.getMaxRange());
    }
}