package org.ffernanl;

public class Thing {
     int health;
     boolean isAlive;
     int position;

    public Thing() {
        this.health = 1;
        isAlive = true;
        position = 0;
    }

    public Thing(int health, int position) {
        this.health = health;
        isAlive = true;
        this.position = position;
    }


    public void destroy(){
        if(health < 1) {
            health = 0;
            isAlive = false;
        }
    }


}
