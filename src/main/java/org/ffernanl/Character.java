package org.ffernanl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Character {

    private int health;
    private int level;

    private boolean isAlive;

    private int maxRange;

    private FightType fightType;

    private int position;

    private Set<Faction> factions;


    public Character(int health, int level, boolean isAlive, int maxRange, FightType fightType, int position, Set<Faction> factions) {
        this.health = health;
        this.level = level;
        this.isAlive = isAlive;
        this.maxRange = maxRange;
        this.fightType = fightType;
        this.position = position;
        this.factions = factions;
    }

    public Character(){
        health = 1000;
        level = 1;
        isAlive = true;
        position = 0;
        this.fightType = FightType.MELEE;
        maxRange = calculateMaxRange();
        factions = new HashSet<>();
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isAlive() {
        if(!isAlive) health = 0;
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

    public FightType getFightType() {
        return fightType;
    }

    public void setFightType(FightType fightType) {
        this.fightType = fightType;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Set<Faction> getFactions() {
        return factions;
    }

    public void setFactions(Set<Faction> factions) {
        this.factions = factions;
    }

    public boolean heal(Character characterHealed) {
        if ((this == characterHealed || isAlly(characterHealed)) && this.isAlive && characterHealed.health < 1000) {
            characterHealed.health = 1000;
            return true;
        }
        return false;
    }

    public void dealDamage(int damageDone, Character characterDamaged){
        if(this != characterDamaged && !isAlly(characterDamaged)) {
                damageDone = calculateDamage(damageDone, characterDamaged);
                if (characterDamaged.health <= damageDone)
                    characterDamaged.isAlive = false;
                else
                    characterDamaged.health -= damageDone;

            }
    }

    public void dealDamage(int damageDone, Thing thing){
        if(isAttackable(thing)) {
            if (thing.health <= damageDone)
                thing.destroy();
            else
                thing.health -= damageDone;
        }
        }

    public int calculateDamage(int damageDone, Character characterDamaged){
        int levelDiff = this.level - characterDamaged.getLevel();

        if(isAttackable(characterDamaged)) {
            if (levelDiff <= -5)
                damageDone /= 2;
            else if (levelDiff >= 5)
                damageDone = damageDone + damageDone / 2;

            return damageDone;
        }
        return 0;
    }

    public int calculateMaxRange(){
        if(fightType.equals(FightType.RANGE))
            return 20;

        //Default -> MELEE
        return 2;
    }

    public boolean isAttackable(Character character2){
        return character2.position - this.position <= maxRange;
    }
    public boolean isAttackable(Thing thing){
        return thing.position - this.position <= maxRange;
    }


    public boolean joinFaction(Faction factionP){
        for (Faction actual: factions) {
            if(factionP.getName().equals(actual.getName()))
                return false;
        }

        factions.add(factionP);
        return true;
    }

    public void leaveFaction(Faction factionP) throws FactionNotExistsException {
            if(!factions.contains(factionP))
                throw new FactionNotExistsException("Player doesn't have this faction");

        factions.remove(factionP);
    }

    public boolean isAlly(Character ch2){
        for (Faction factionActual: factions) {
            for (Faction factionActualCh2: ch2.getFactions()) {
                if(factionActual.getName().equals(factionActualCh2.getName()))
                    return true;
            }
        }
        return false;
    }


}
